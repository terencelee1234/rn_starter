import React, {Component} from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  SafeAreaView,
} from 'react-native';
import {Provider} from 'react-redux';
import {StyleProvider, Spinner} from 'native-base';
import AppNavigator from './js/components/AppNavigator';
import getTheme from './native-base-theme/components';
import platform from './native-base-theme/variables/platform';
import configureStore from './js/configureStore';


export default class App extends Component<{}> {
  constructor() {
    super();
    this.store = configureStore();
  }
  render() {
    return (
      // <SafeAreaView style={styles.safeArea}>
      <StyleProvider style={getTheme(platform)}>
        <Provider store={this.store}>
          <AppNavigator />
        </Provider>
      </StyleProvider>
      // </SafeAreaView>
    );
  }
}

