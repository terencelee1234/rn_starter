import {Record} from 'immutable';
import {BaseRedux} from './reducers/base';

export type ListType = {key:string};

export type REDUCER_STATE = {
  base:BaseRedux
}

export type RecordType<T> = T & Record<T>;
