import {Dimensions, Platform} from 'react-native';

const d = Dimensions.get('window');

export const DIMENSION = {
  screenWidth: d.width,
  screenHeight: d.height,
  drawerWidth: d.width - 100,
};

export const OS = Platform.OS;

export const IS_IPHONE_X = Platform.OS === 'ios' && DIMENSION.screenHeight === 812 && DIMENSION.screenWidth === 375;

export const IS_NORMAL_IOS = Platform.OS === 'ios' && !(DIMENSION.screenHeight === 812 && DIMENSION.screenWidth === 375);

