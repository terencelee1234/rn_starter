
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  Platform,
  StyleSheet,
  View,
  WebView,
} from 'react-native';
import Touchable from 'react-native-platform-touchable';

import {Container, Button, Text, Icon} from 'native-base';
import {REDUCER_STATE} from '../../types';
import {reduxAddAsync, reduxAdd} from '../../reducers/base';
import NormalHeader from '../general/NormalHeader';
import SideMenu from '../side-menu/SideMenu';
import TopLeftToolbarButton from '../general/TopLeftToolbarButton';
import theme from '../../../native-base-theme/variables/platform';
import {openDrawer, closeDrawer, toggleDrawer} from '../side-menu/controller';

const menuIcon = require('../../../assets/images/menu.png');
const notificationIcon = require('../../../assets/images/notification.png');

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

export type HomeSceneExternalProps = {

}

export interface HomeSceneProps extends HomeSceneExternalProps {
  counter: number,
  addCounter: () => void,
  addCounterAsync: () => void,
}

class HomeScene extends Component<HomeSceneProps> {
  componentWillMount() {
  }
  componentWillReceiveProps(nextProps:HomeSceneProps) {
  }
  render() {
    return (
      <SideMenu menu={<Container />} isLeft={false}>
        <SideMenu menu={<Container />} isLeft>
          <Container style={styles.container}>
            <NormalHeader
              title="HOME"
              leftComponent={
                <TopLeftToolbarButton
                  onPress={() => {
                    this.props.toggleDrawer();
                    return true;
                  }}
                  source={menuIcon}
                />
              }
              rightComponent={
                <TopLeftToolbarButton
                  onPress={() => {
                    this.props.toggleDrawer(false);
                    return true;
                  }}
                  source={notificationIcon}
                />
              }
            />
            <Text>{this.props.counter}</Text>
            <Button onPress={this.props.addCounter}>
              <Text>Add</Text>
            </Button>
            <Button onPress={this.props.addCounterAsync}>
              <Text>Add Async</Text>
            </Button>
          </Container>
        </SideMenu>
      </SideMenu>
    );
  }
}

const bindAction = (dispatch, props:HomeSceneExternalProps) => {
  return {
    addCounter: () => {
      dispatch(reduxAdd());
    },
    addCounterAsync: () => {
      dispatch(reduxAddAsync());
    },
    openDrawer: (isLeft) => {
      dispatch(openDrawer(isLeft));
    },
    closeDrawer: (isLeft) => {
      dispatch(closeDrawer(isLeft));
    },
    toggleDrawer: (isLeft) => {
      dispatch(toggleDrawer(isLeft));
    },
  };
};

const mapStateToProps = (state:REDUCER_STATE, props:HomeSceneExternalProps) => {
  return {
    counter: state.base.counter,
  };
};

export default connect(mapStateToProps, bindAction)(HomeScene);
