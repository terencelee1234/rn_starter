import React, {Component} from 'react';

import {
  StyleSheet,
  View,
} from 'react-native';
import {
  Container,
  List,
  ListItem,
  Text,
} from 'native-base';
import {Actions} from 'react-native-router-flux';

import theme from '../../../native-base-theme/variables/platform';
import {DIMENSIONS} from '../../constants';

const styles = StyleSheet.create({
});

type LeftDrawerProps = {
}

class LeftDrawer extends Component<LeftDrawerProps> {
  state = {
  }
  render() {
    return (
      <Container style={{width: DIMENSIONS.drawerWidth + 10}}>
        <View style={{height: 80}} />
        <ListItem itemHeader />
        <ListItem>
          <Text>Exchange</Text>
        </ListItem>
        <ListItem>
          <Text>Settings</Text>
        </ListItem>
        <ListItem onPress={Actions.contactList}>
          <Text>Contacts</Text>
        </ListItem>
      </Container>
    );
  }
}

export default LeftDrawer;
