import React, {Component} from 'react';

import {
  StyleSheet,
} from 'react-native';
import {
  Container,
} from 'native-base';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {Map} from 'immutable';
import RNSideMenu from 'react-native-side-menu';

import theme from '../../../native-base-theme/variables/platform';
import {REDUCER_STATE} from '../../configureStore';
import {openDrawer, closeDrawer} from './controller';
import {DIMENSION} from '../../common/constants';

const styles = StyleSheet.create({
});

type SideMenuProps = {
  isLeft:boolean,
  menu:any,
}

interface SideMenuExProps extends SideMenuProps {
  isOpen:boolean,
  openDrawer:() => void,
  closeDrawer:() => void,
}

class SideMenu extends Component<SideMenuExProps> {
  state = {
  }
  key = (this.props.isLeft) ? 'isLeftOpen' : 'isRightOpen';
  // shouldComponentUpdate(nextProps:SideMenuExProps, nextState) {
  //   return this.props.isOpen !== nextProps.isOpen;
  // }
  render() {
    const {isLeft, isOpen, menu} = this.props;
    let menuPosition = (isLeft) ? 'left' : 'right';
    return (
      <RNSideMenu
        isOpen={isOpen}
        menu={menu}
        openMenuOffset={DIMENSION.drawerWidth}
        onChange={(newIsOpen) => {
          if (newIsOpen) {
            this.props.openDrawer(isLeft);
          } else {
            this.props.closeDrawer(isLeft);
          }
        }}
        menuPosition={menuPosition}
        bounceBackOnOverdraw={false}
      >
        {this.props.children}
      </RNSideMenu>
    );
  }
}

const bindAction = (dispatch, props:SideMenuProps) => {
  return {
    openDrawer: (isLeft:boolean) => {
      dispatch(openDrawer(isLeft));
    },
    closeDrawer: (isLeft:boolean) => {
      dispatch(closeDrawer(isLeft));
    },
  };
};

const mapStateToProps = (state:REDUCER_STATE, props:SideMenuProps) => {
  const key = (props.isLeft) ? 'isLeftOpen' : 'isRightOpen';
  return {
    isOpen: state.sideMenu.get(key),
  };
};

export default connect(mapStateToProps, bindAction)(SideMenu);
