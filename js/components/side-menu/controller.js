import {Map, List} from 'immutable';

export const EX = 'EX';
export const CLOSE_DRAWER = 'CLOSE_DRAWER';
export const OPEN_DRAWER = 'OPEN_DRAWER';
export const TOGGLE_DRAWER = 'TOGGLE_DRAWER';

const initialState = Map({
  test: '',
  isLeftOpen: false,
  isRightOpen: false,
});

export function example(test:string) {
  return {
    type: EX,
    payload: {
      test,
    },
  };
}

export function exampleAsync() {
  return async (dispatch, getState) => {
    console.log(getState());
    dispatch(example('test'));
  };
}

// position: true = left, false = right
export function openDrawer(isLeft:boolean = true) {
  return {
    type: OPEN_DRAWER,
    isLeft,
  };
}

export function closeDrawer(isLeft:boolean = true) {
  return {
    type: CLOSE_DRAWER,
    isLeft,
  };
}

export function toggleDrawer(isLeft:boolean = true) {
  return {
    type: TOGGLE_DRAWER,
    isLeft,
  };
}

export function reducer(state = initialState, action) {
  let newState = state;
  let key = (action.isLeft) ? 'isLeftOpen' : 'isRightOpen';
  try {
    switch (action.type) {
      case EX:
        newState = newState.set('test', action.payload.test);
        break;
      case OPEN_DRAWER:
        newState = newState.set(key, true);
        break;
      case CLOSE_DRAWER:
        newState = newState.set(key, false);
        break;
      case TOGGLE_DRAWER:
        newState = newState.updateIn([key], (o) => {
          return !o;
        });
      default:
        break;
    }
  } catch (e) {
    console.log(e);
  }
  return newState;
}
