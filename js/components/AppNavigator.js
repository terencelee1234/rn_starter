
import React, {Component} from 'react';
import {StatusBar, Text, Platform, UIManager, PermissionsAndroid, Linking} from 'react-native';
import {connect} from 'react-redux';
import {Container} from 'native-base';
import {Router, Scene, Reducer, Actions} from 'react-native-router-flux';
import HomeScene from './home/HomeScene';

const CustomRouter = props => (
  <Router
    {...props}
    createReducer={(params) => {
      const defaultReducer = new Reducer(params);
      return (state, action) => {
        props.dispatch(action);
        return defaultReducer(state, action);
      };
    }}
  />
);

const ReduxRouter = connect()(CustomRouter);

class AppNavigator extends Component {
  componentWillMount() {
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  componentWillReceiveProps() {
  }

  render() {
    return (
      <Container>
        <ReduxRouter
          onExitApp={() => true}
          backAndroidHandler={() => {
            if (Actions.state.index === 0) {
              return true;
            }
            Actions.pop();
            return true;
          }}
        >
          <Scene key="root">
            <Scene key="home" initial hideNavBar component={HomeScene} />
          </Scene>
        </ReduxRouter>
      </Container>
    );
  }
}


export default AppNavigator;
