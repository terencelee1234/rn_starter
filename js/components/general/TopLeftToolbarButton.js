import React, {Component} from 'react';
import {
  StyleSheet,
  Image as RNImage,
  TouchableWithoutFeedbackProperties,
  ImageProperties,
  Platform,
  Dimensions,
} from 'react-native';
import {
  Button,
  Label,
  Text,
} from 'native-base';
import Touchable from 'react-native-platform-touchable';
import {Actions} from 'react-native-router-flux';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const isIphoneX = Platform.OS === 'ios' && deviceHeight === 812 && deviceWidth === 375;


const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 5,
    paddingTop: (Platform.OS === 'ios' && !isIphoneX) ? 10 : 0,
  },
});

type TopLeftToolbarButtonProps = {
  touchableProps:TouchableWithoutFeedbackProperties,
  imageProps:ImageProperties,
  onPress:() => void,
}

const TopLeftToolbarButton = (props:TopLeftToolbarButtonProps = {onPress: Actions.pop}) => {
  return (
    <Touchable onPress={props.onPress} style={styles.container} {...props.touchableProps}>
      <RNImage style={{height: 20, width: 20}} source={props.source} {...props.imageProps} />
    </Touchable>
  );
};

export default TopLeftToolbarButton;
