import React, {Component} from 'react';
import {
  Image as RNImage,
  ViewStyle,
  Platform,
} from 'react-native';
import {
  Header,
  Left,
  Right,
  Body,
  Title,
} from 'native-base';
import {Actions} from 'react-native-router-flux';

import BackButton from './BackButton';
import {IS_NORMAL_IOS} from '../../common/constants';

type Props = {
  title:string,
  canPop:boolean,
  onLeftPress:() => boolean,
  leftComponent:any,
  rightComponent:any,
  centerComponent:any,
  styles: {
    header: ViewStyle,
  },
}

class NormalHeader extends Component<Props> {
  static defaultProps = {
    styles: {},
  }
  onLeftPress = () => {
    const {onLeftPress} = this.props;
    console.log('...');
    if (onLeftPress && onLeftPress()) {
      console.log('shit');
      return;
    }
    Actions.pop();
  }
  render() {
    let {canPop, leftComponent, rightComponent, centerComponent} = this.props;
    if (!leftComponent && canPop) leftComponent = <BackButton onPress={this.onLeftPress} />;
    const materialLeftStyle = (Platform.OS === 'android' && (canPop || leftComponent)) ? {
      flex: null,
      width: 72,
    } : null;
    return (
      <Header style={this.props.styles.header}>
        <Left style={materialLeftStyle}>
          {leftComponent}
        </Left>
        {
          (centerComponent) || (
            <Body style={{flexGrow: 5}}>
              <Title style={{paddingTop: (IS_NORMAL_IOS) ? 10 : 0}}>{this.props.title}</Title>
            </Body>
          )
        }
        <Right style={{flexGrow: 1}}>
          {rightComponent}
        </Right>
      </Header>
    );
  }
}
export default NormalHeader;
