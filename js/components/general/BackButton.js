import React, {Component} from 'react';
import {
  StyleSheet,
  Image as RNImage,
  TouchableWithoutFeedbackProperties,
  ImageProperties,
} from 'react-native';
import {
  Button,
  Label,
  Text,
} from 'native-base';
import Touchable from 'react-native-platform-touchable';
import {Actions} from 'react-native-router-flux';
import TopLeftToolbarButton from './TopLeftToolbarButton';

const arrowLeftIcon = require('../../../assets/images/arrow-left.png');

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

type BackButtonProps = {
  touchableProps:TouchableWithoutFeedbackProperties,
  imageProps:ImageProperties,
  onPress:() => void,
}

const BackButton = (props:BackButtonProps = {onPress: Actions.pop}) => {
  return (
    <TopLeftToolbarButton onPress={props.onPress} source={arrowLeftIcon} />
  );
};

export default BackButton;
