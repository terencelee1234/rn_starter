
import {AsyncStorage} from 'react-native';
import devTools from 'remote-redux-devtools';
import {createStore, applyMiddleware, combineReducers} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import {persistStore, autoRehydrate} from 'redux-persist';

import promise from './promise';

import base from './reducers/base';
import {reducer as sideMenu} from './components/side-menu/controller';

const reducers = combineReducers({
  base,
  sideMenu,
});

export default function configureStore(onCompletion:()=>void):any {
  const enhancer = composeWithDevTools(
    applyMiddleware(thunk, promise),
    autoRehydrate(),
  );

  const store = createStore(reducers, enhancer);
  persistStore(store, {storage: AsyncStorage, whitelist: []}, onCompletion);

  // if (module.hot) {
  //   module.hot.accept(() => {
  //     const nextRootReducer = require('./reducers/index').default;
  //     store.replaceReducer(nextRootReducer);
  //   });
  // }

  return store;
}
