import {Record} from 'immutable';
import {RecordType} from '../types';

export const ADD_ACTION = 'ADD_ACTION';
export const ADD_ASYNC_ACTION = 'ADD_ASYNC_ACTION';

export const reduxAdd = () => {
  return {
    type: ADD_ACTION,
  };
};

export const reduxAddAsync = () => {
  return async (dispatch, state) => {
    setTimeout(() => {
      dispatch(reduxAdd());
    }, 1000);
  };
};

const defaultBaseRedux = {
  counter: 0,
};

type BaseReduxProps = {
  counter: number,
}

const BaseReduxFactory = Record(defaultBaseRedux);

const initialState:RecordType<BaseReduxProps> = BaseReduxFactory();

export default (state = initialState, action) => {
  let newState = state;
  switch (action.type) {
    case ADD_ACTION:
      newState = newState.update('counter', (c) => {
        return c + 1;
      });
      break;
    case ADD_ASYNC_ACTION:
      newState.counter += 1;
      break;
    default:
  }
  return newState;
};
