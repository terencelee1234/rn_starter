import variable from './../variables/platform';

export default (variables = variable) => {
  const leftTheme = {
    flex: null,
    alignSelf: 'center',
    alignItems: 'flex-start',
  };

  return leftTheme;
};
