import variable from './../variables/platform';

export default (variables = variable) => {
  const labelTheme = {
    '.focused': {
      width: 0,
    },
    color: variable.textMediumGrey,
    fontSize: 17,
  };

  return labelTheme;
};
