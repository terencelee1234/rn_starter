import variable from './../variables/platform';

export default (variables = variable) => {
  const textTheme = {
    fontSize: variables.DefaultFontSize - 1,
    fontFamily: variables.fontFamily,
    color: variables.textColor,
    fontWeight: '300',
    '.note': {
      color: variable.textLightGrey,
      fontSize: variables.noteFontSize,
    },
  };

  return textTheme;
};
