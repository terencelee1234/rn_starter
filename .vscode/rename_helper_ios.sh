#!/usr/bin/env bash
echo $1
cd ../ios
for INPUT in $(ls | grep rn_starter)
do
  mv $INPUT "${INPUT//rn_starter/$1}"
done