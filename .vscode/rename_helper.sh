#!/usr/bin/env bash
cd "$(dirname "$0")"

for INPUT in $(echo $1 | tr "." "\n")
do
  BUNDLE_ID="${INPUT}"
done

./rename_helper_android.sh $1
./rename_helper_ios.sh $BUNDLE_ID